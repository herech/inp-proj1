-- Author: Jan Herec, xherec00@fit.vutbr.cz
-- Date of creation: 18. 10. 2014
-- Description: Control of matrix display from FPGA

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ledc8x8 is
    port(
        -- input signals
        SMCLK : in std_logic;
        RESET : in std_logic;
        -- output signals
        ROW : out std_logic_vector(0 to 7); -- I use "to" instead of the classic "downto", because I want to make difference for VHD synthesizer between signal ROW and curr_row
        LED : out std_logic_vector(0 to 7) -- Within the consistency of out signals is used "to" instead of classic "downto"
    );
end entity ledc8x8;

architecture behavioral of ledc8x8 is
    -- current row vector
    signal curr_row : std_logic_vector(7 downto 0);
    -- clock enable counter
    signal ce_cnt : std_logic_vector(7 downto 0);
    -- clock enable flag
    signal ce : std_logic;
begin
    
    -- clock enable generator
    ce_gen : process(SMCLK, RESET, ce_cnt) 
    begin    
        -- asynchronous reset initializes or reinitializes signals, which are used in clock enable generator process
        if (RESET = '1') then
            ce_cnt <= (others => '0');
            ce <= '0';
        -- in each clock cycle we increment clock enable counter
        elsif (SMCLK'event and SMCLK = '1') then
            ce_cnt <= ce_cnt + 1;
        end if;
        
        -- set clock enable flag to 1, if time period 1 / (SMCLK / 256) elapsed, otherwise set clock enable flag to 0
        -- time period begins when the value of the clock enable counter equals to 1 
        if (ce_cnt = X"01") then
            ce <= '1';
        else 
            ce <= '0';
        end if;
    end process ce_gen;
    
    -- activation diods in current row
    act_diods : process(SMCLK, ce, RESET)
    begin
        -- asynchronous reset initializes or reinitializes signals, which are used in diod's activation process
        if (RESET = '1') then
            curr_row <= "00000001";
            ROW <= "00000000";
            LED <= "11111111";
            
        elsif (SMCLK'event and SMCLK = '1') then
            -- if SMCLK'event occured and clock are enabled, we can activate diods in current row
            if (ce = '1') then
                -- determination of next row (in principle we rotate by rows upwards)
                -- curr_row value changes to next row after this process
                if (curr_row = "10000000") then
                    curr_row <= "00000001";
                else
                    curr_row <= curr_row(6 downto 0) & '0';
                end if;
                
                -- set current row
                ROW <= curr_row;
                
                -- set active diods in current row. 
                -- because we rotate by rows upwards, we start with last row
                case curr_row is
                    when "00000001" =>
                        LED <= "11110110";
                    when "00000010" =>
                        LED <= "11110110";
                    when "00000100" =>
                        LED <= "11110000";
                    when "00001000" =>
                        LED <= "10010110";
                    when "00010000" =>
                        LED <= "01100110";
                    when "00100000" =>
                        LED <= "11101111";
                    when "01000000" =>
                        LED <= "11101111";
                    when "10000000" =>
                        LED <= "11101111";
                    when others =>
                        LED <= "11111111";
                end case;
            end if;   
        end if;
    end process act_diods;
        
end architecture behavioral;